﻿using System;
using System.Diagnostics;
using System.Security.Permissions;
using System.Threading;
using System.Threading.Tasks;

namespace Xpdf
{
    public static class XPdf
    {
        public delegate void FinishEvent();
        public static event FinishEvent Finish;

        private static Thread currentThread;
        private static Task currentTask;
        private static CancellationTokenSource canceller;

        public static bool InProgress { get; private set; }

        [SecurityPermissionAttribute(SecurityAction.Demand, ControlThread = true)]
        public static async void _FromURL(string url, string filename)
        {
            if (InProgress)
                return;

            InProgress = true;

            await _pFromURL(url, filename);

            while (currentTask.Status == TaskStatus.Running)
                Thread.Sleep(1);

            currentTask.Dispose();

            InProgress = false;
        }

        private static Task _pFromURL(string url, string filename)
        {
            canceller = new CancellationTokenSource();

            currentTask = Task.Factory.StartNew(() =>
            {
                currentThread = Thread.CurrentThread;
                new Xpdf.XPdfer().UrlToPdf(new System.Security.Policy.Url(url), filename);

                _finished();
                Finish?.Invoke();
            }, canceller.Token);

            return currentTask;
        }

        #region En attente
        public static async void FromURL(string url, string filename)
        {
            if (InProgress)
                return;

            InProgress = true;

            await private_FromURL(url, filename);
        }

        public static async void FromHTML(string html, string filename)
        {
            if (InProgress)
                return;

            InProgress = true;

            await private_FromHTML(html, filename);
        }

        private static Task private_FromURL(string url, string filename)
        {
            canceller = new CancellationTokenSource();

            currentTask = Task.Factory.StartNew(() =>
            {
                currentThread = Thread.CurrentThread;
                new Xpdf.XPdfer().UrlToPdf(new System.Security.Policy.Url(url), filename);

                _finished();
                Finish?.Invoke();
            }, canceller.Token);

            return currentTask;
        }

        private static Task private_FromHTML(string html, string filename)
        {
            canceller = new CancellationTokenSource();

            currentTask = Task.Factory.StartNew(() =>
            {
                currentThread = Thread.CurrentThread;
                new Xpdf.XPdfer().HtmlToPdf(html, filename);

                _finished();
                Finish?.Invoke();
            }, canceller.Token);

            return currentTask;
        }
        #endregion

        //[SecurityPermissionAttribute(SecurityAction.Demand, ControlThread = true)]
        private static void _finished()
        {
            canceller.Cancel();
        }
    }
}

﻿using System;
using System.Threading.Tasks;

namespace ConsoleTest
{
    public static class Test
    {
        private static Task send()
        {
            Console.WriteLine("wait");
            return Task.Delay(15000);
        }

        public static async void DoTest()
        {
            await send();
        }
    }
}
